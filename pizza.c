/* 

This program simulates a pizza shop with a chef produing pizza and customers eating them.
The chef can only make certain number of pizza, and customers can buy pizza only if there 
is pizza available. Customer can only start eating if there is a seat available, where 
there are a limited number of seats. For a specified total number of customers , the program 
terminates if all customers finish eating their pizza.

1. Extract the zip file and move the working directory to where the extracted files are.

2. To compile the program, type 

% gcc pizza.c -o pizza -pthread

This code compiles in Linux but not in Mac OS X.

3. The program takes two command line arguments. The first one corresponds to "M", or number of 
   plates for pizza, in the problem; the second one corresponds to "N", or the number of seats.
   For example, if M=5 and N=8 are the desired values, type the following to execute the program.

$ ./pizza 5 8

*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#define MAX_CUSTOMER 20
#define COOK_PIZZA_TIME 2 
#define MAX_ARRIVAL_TIME 30
#define MAX_EAT_PERIOD 10
#define TIME_FACTOR 1000000

//global variables shared by all threads
int pizzaCount = 0;
int seatCount = 0;
int customers = 0;
int customers_served = 0;

sem_t pizzaMutex; //binary semaphore for pizza
sem_t pizza; //counting semaphore for pizza
sem_t plate; //counting semaphore for plates
sem_t seatMutex; // binary semaphore for seating
sem_t seat;    // counting semaphore for seats

int put_to   = 1; //pointer to the next plate to put pizza

int get_from   = 1; //pointer to the next plate to get pizza

// data structure for passing data (plates and seats) to threads
typedef struct
{
    long long threadId;
    int plates;
    int seats;
    int customer_num;
} thdata;

// define semaphores
sem_t pizzMutex; //binary semaphore for pizza
sem_t pizza; //counting semaphore for pizza
sem_t seatMutex; // binary semaphore for seating
sem_t seat;    // counting semaphore for seats

//start routine for the chef thread, a thdata struct is passed as parameter
void *chef(void *ptr)
{ 
  // type cast to a pointer to thdata 
  thdata * data;
  data = (thdata *) ptr;
  // number of plates
  int M = data->plates;
  int customer_num = data->customer_num;
  
  // Keep making pizza if the number of unsold pizza is few than the customer still not served
  while(pizzaCount < (customer_num - customers_served)){  

  // Block if there are no plates for pizza
  sem_wait(&plate);

  // Block if customer is taking pizza
  sem_wait(&pizzaMutex);
  // put pizza on the next empty plate
  // takes COOK_PIZZA_TIME time units
  int i;
  for (i = 0; i < (COOK_PIZZA_TIME * TIME_FACTOR); i++);
  pizzaCount ++;
  printf("Chef is making a pizza %d, put in plate %d. %d pizza unsold.\n", ((put_to-1) % M)+1, ((put_to-1) % M)+1,   pizzaCount);
  put_to = (put_to % M) + 1;
  // increase the total number of pizza by 1
 
  // print out that the chef's made a pizza, the plate it's put in, and the current number of unsold pizza
  
  // release the pizza mutex lock
  sem_post(&pizzaMutex);
   
  // release the pizza counting semaphore
  sem_post(&pizza);
  }
  // Stops making pizza if there is enough pizza left for all remaining customers.
  pthread_exit(NULL);
}

//start routine for the customer threads. A thdata struct is passed as parameter 
void *customer(void *ptr){

  // type cast to a pointer to thdata 
  thdata * data;
  data = (thdata *) ptr;
  int M = data->plates;
  int N = data->seats;
  int id = data->threadId; 

  // set random seed
  srand48(time(0));
  
  // arrive after a period between 0 to MAX_ARRIVAL_TIME time units
  int randnum;	
  randnum = (int)(drand48() * MAX_ARRIVAL_TIME);
  int i;
  for (i = 0; i < randnum * TIME_FACTOR; i++);  
  // block if there is no pizza
  sem_wait(&pizza);
  // wait for lock to get pizza
  sem_wait(&pizzaMutex);
  // decrease the number of unsold pizza by 1
  pizzaCount--;
  customers_served++;	
 //print information 
  printf("Customer %d is buying a pizza, gets pizza %d from plate %d. %d pizza unsold.\n", id, ((get_from-1) % M)+1, ((get_from-1) % M)+1, pizzaCount);
  get_from = (get_from % M) + 1;
 
  // release lock
  sem_post(&pizzaMutex);
  // release the plate counting semphore
  sem_post(&plate);
  // wait for seat
  sem_wait(&seat);
  // acquire seat mutex lock
  sem_wait(&seatMutex);
  // take seat, decrease the number of available seats by 1
  seatCount--;  
  // print information
  printf("Customer %d gets a seat and is eating. %d seats left.\n", id, seatCount);
  sem_post(&seatMutex);
  // eat for a period of time between 1 and MAX_EAT_PEROID time units
  
  // set random seed
  srand48(time(0));
    
  randnum = (int)(drand48() * (MAX_EAT_PERIOD-1));
  for (i = 0; i < (randnum + 1) * TIME_FACTOR; i++);  
  // acquire the seat mutex lock
  sem_wait(&seatMutex);
  // leave seat, increase number of available seats and number of served customers by 1
  seatCount++;
 
  printf("Customer %d finishes eating and is leaving. %d seats left\n", id, seatCount);
  // release seat mutex
  sem_post(&seatMutex);
  // release seat counting semaphore
  sem_post(&seat);
  // terminate thread
  pthread_exit(NULL);
}

int main (int argc, char *argv[])
{ 

   // read input parameters
  if(argc != 3) {
      printf("USAGE: %s <Number of plates for pizza> <Number of seats>\n",argv[0]);
      exit(-1);
  } 
  else{
      // number of plates for holding pizza
      int M = atoi(argv[1]);
      // number of seats
      int N = atoi(argv[2]);
      
      // maximum number for M or N is 10 
      if ((M>10) || (N>10)){
        printf("Arguments should not exceed 10.\n");
        exit(-1);
      }
      else{
        // number of plates for holding pizza
        int M = atoi(argv[1]);
        // number of seats
        int N = atoi(argv[2]);
        
        printf("M: %d N: %d\n",M,N);

        seatCount = N;

        // set random seed
        srand48(time(0));
        // randomly generate the number of customers
        customers = 1 + (int)(drand48() * (MAX_CUSTOMER-1));
        printf("The total number of customers is %d.\n", customers);

        // declare a thread for the chef
        pthread_t chefThread;

        // declare a thread array for customers 
        pthread_t customerThreads[customers];
        long long i; 
        int status;

       //initialize semaphores
       sem_init(&pizzaMutex,0,1);
       sem_init(&seatMutex,0,1);
       sem_init(&plate, 0, M);	
       sem_init(&pizza, 0, 0); 
       sem_init(&seat, 0, N); 

     
       // declare thdata struct for passing data to chef thread
       thdata chef_data;
       chef_data.plates = M;
       chef_data.customer_num = customers;
       // declare thdata structs for passing data to customer thread
       thdata customer_data[customers];   
       for (i = 0; i<customers; i++){
         customer_data[i].threadId = i+1;
         customer_data[i].plates = M;
         customer_data[i].seats = N;
       }


      for (i = 0; i<customers; i++){
        if ( pthread_create(&customerThreads[i], NULL, customer, (void *) &customer_data[i]) != 0) {
           // It returns 0 on success and -1 on error.
      	   perror("pthread create error! \n");
      	   exit(-1);
        }
      }
      // create a thread for chef
      if ( pthread_create(&chefThread, NULL, chef, (void *)&chef_data) != 0) {
        // It returns 0 on success and -1 on error.
        perror("pthread create error! \n");
        exit(-1);
      }

      if (pthread_join(chefThread, (void **)&status) != 0) {
        //Suspend execution of the calling thread until the target thread terminates.      
        // It returns 0 on success, and -1 on error
      	 perror(" pthread join error\n");
      	 exit(-1);
      }
           //printf("Completed join with chef thread. Status= %d\n", status);
         
      for(i = 0; i<customers; i++) {
          //Suspend execution of the calling thread until the target thread terminates.
          // The returned status is stored in variable status.
          if (pthread_join(customerThreads[i], (void **)&status) != 0) {
            // It returns 0 on success, and -1 on error
    	      perror(" pthread join error\n");
    	     exit(-1);
          }
           //printf("Completed join with thread %lld status= %d\n",i, status);
      }

      sem_destroy(&pizza);
      sem_destroy(&seat);
      sem_destroy(&pizzaMutex);
      sem_destroy(&seatMutex);
    }
 }

return 0;
  
}


