#!usr/bin/perl
use strict;

use Bio::Seq;
use Bio::SimpleAlign;
use Bio::EnsEMBL::Registry;
use Bio::EnsEMBL::Compara::DBSQL::DBAdaptor;

mkdir ("reference_sequence1");
mkdir ("reference_alignments");
mkdir ("non_reference_alignments");
my $i;
my $NUM_CHROM = 20;

for ($i=1; $i<=$NUM_CHROM; $i++){
  get_ref_aligns ($i, "./reference_sequence1/".$i."_ref_sequence","./reference_alignments/".$i."_ref_align","./non_reference_alignments/".$i."_ref_align");
}

sub get_ref_aligns 
{
  my ($data_file,$seq_output_file, $ref_output_file,$non_output_file)=@_;

  my $host   = 'sugarman';
  my $user  = 'ensembl';
  my $pass   = 'ensembl';
  my $dbname = 'ensembl_compara_61';
  # chromosome number

  my $seq_region;

  if ($data_file == 20) {
    $seq_region = "X";
  }
  elsif ($data_file == 21){
    $seq_region = "Y";
  }
  else {
    $seq_region = $data_file;
  }

  # The organisms used in this comparative study are
  # Mouse (reference)
  # Human (9606)
  # Rat   (10116)
  # Horse (9796)
  # Dog   (9615)

  my $registry = 'Bio::EnsEMBL::Registry';

  my $seq_output;
  my $ref_output;
  my $non_output;

  $registry->load_registry_from_db(
      -host => $host,
      -user => $user,
      -pass => $pass
  );

  my $comparadb= new Bio::EnsEMBL::Compara::DBSQL::DBAdaptor(
      -host   => $host,
      -user   => $user,
      -dbname => $dbname,
  );

  ## Get adaptors.

  my $genomic_align_block_adaptor = Bio::EnsEMBL::Registry->get_adaptor(
      'Multi', 'compara', 'GenomicAlignBlock');

  my $genomic_align_adaptor = Bio::EnsEMBL::Registry->get_adaptor(
      'Multi', 'compara', 'GenomicAlign');

  my $method_link_species_set_adaptor = Bio::EnsEMBL::Registry->get_adaptor(
      'Multi', 'compara', 'MethodLinkSpeciesSet');

  my $epo_mammals_mlss =
      $method_link_species_set_adaptor->fetch_by_method_link_type_species_set_name(
          "EPO",
          "mammals"
      );
  my $query_species = 'Mouse';

  # Getting the Slice adaptor:
  my $slice_adaptor = Bio::EnsEMBL::Registry->get_adaptor(
      $query_species, 'core', 'Slice');

  my $fh = open_file("./utr_by_chromosome/$data_file");

  while (my $line=<$fh>){
    my @list = split(/\s+/,$line);
    my $seq_start = @list[1];
    my $seq_end   = @list[2];

    my $this_seq;
    # print $seq_start,"   ",$seq_end,"\n";

    # Fetching a Slice object:
    if (@list[3] eq "+"){
      my $query_slice = $slice_adaptor->fetch_by_region(
        'toplevel',
        $seq_region,
        $seq_start-2,
        $seq_end);

      $this_seq=$query_slice->seq();

      my $genomic_align_blocks =
        $genomic_align_block_adaptor->fetch_all_by_MethodLinkSpeciesSet_Slice(
          $epo_mammals_mlss,
    	$query_slice);

      foreach my $this_block(@$genomic_align_blocks) {

        my $restricted_block = $this_block->restrict_between_reference_positions($seq_start-2, $seq_end);

        my @non_ref = get_alignment_array($restricted_block);

        if (scalar(@non_ref) == 4){

          #	print scalar(@non_ref),"\n";

          my $this_ref_align = $restricted_block->reference_genomic_align();

          my $this_ref_aligned_seq = $this_ref_align->aligned_sequence();

          $ref_output = $ref_output."$this_ref_aligned_seq\n";
          }

          else {
    	       next;
          }
      }
    }

    elsif (@list[3] eq "-"){

      my $query_slice = $slice_adaptor->fetch_by_region(
      'toplevel',
      $seq_region,
      $seq_start,
      $seq_end);

      $this_seq=$query_slice->seq();

      my $genomic_align_blocks =
      $genomic_align_block_adaptor->fetch_all_by_MethodLinkSpeciesSet_Slice(
        $epo_mammals_mlss, $query_slice);

      foreach my $this_block(@$genomic_align_blocks){

        my $restricted_block = $this_block->restrict_between_reference_positions($seq_start,$seq_end);

        my @non_ref = get_alignment_array($restricted_block);

        if (scalar(@non_ref) == 4){

          	my $this_ref_align = $restricted_block->reference_genomic_align();

          	my $this_ref_aligned_seq = $this_ref_align->aligned_sequence();

          	my $rc = scalar reverse ($this_ref_aligned_seq);

          	$rc =~ tr/AGCT/TCGA/;

          	$ref_output = $ref_output."$rc\n";
        }

        else {
    	     next;
        }
      }
    }

    $seq_output.=$this_seq;
  }



  unless (open(SEQ_OUTPUT,">$seq_output_file")){
    print "Cannot open file \"$seq_output_file\" to write to!";
    exit;
  }

  unless (open(REF_OUTPUT,">$ref_output_file")){
    print "Cannot open file \"$ref_output_file\" to write to!";
    exit;
  }

  unless (open(NON_OUTPUT,">$non_output_file")){
    print "Cannot open file \"$non_output_file\" to write to!";
    exit;
  }

  close($fh);

  print SEQ_OUTPUT $seq_output;
  print REF_OUTPUT $ref_output;
  print NON_OUTPUT $non_output;
}

sub get_alignment_array{

  my ($block) = @_;
  my $non_reference_aligns=$block->get_all_non_reference_genomic_aligns();
  my @returned_array;
  my @empty_array;
  my $human;
  my $rat;
  my $horse;
  my $dog;
  my $n=0;
  foreach my $this_align(@$non_reference_aligns){
  	# Get the taxon id for this align
  	my $this_id=$this_align->display_id();
  	my @split_id = split(/:/,$this_id);
  	my $taxon_id=@split_id[0];
  	$taxon_id =~ s/\s+//g;

  	# Check if Human(9606) is contained

  	if ($taxon_id =~ /9606/) {

  	  $human = $this_align->display_id();
  	  push (@returned_array, $human);
  	  
  	}

  	# Check if Rat (10116) is contained
  	elsif ($taxon_id =~ /10116/) {
  	  $rat = $this_align->display_id();
  	  push (@returned_array, $rat);
  	 
  	}

  	# Check if Horse (9796) is contained
  	elsif ($taxon_id =~ /9796/) {
  	  $horse = $this_align->display_id();
  	  push (@returned_array, $horse);
  	 
  	}
  	 
  	# Check if Dog (9615) is contained
  	elsif ($taxon_id =~ /9615/) {
  	  $dog = $this_align->display_id();
  	  push (@returned_array, $dog);
  	}
  	else {
  	  next;
  	}

  }

 # If there are four members in the return_array list, we will return the list. If not, we would return an empty array.
  my $size = scalar(@returned_array);

	if ($size eq 4){
	  return @returned_array;
  } else {
	  return @empty_array;
  }

  return @returned_array;
}

sub open_file {
  my ($filename)=@_;
  my $fh;

  unless(open($fh,$filename)){
    print "Cannot open file $filename\n";
    exit;
  }
  return $fh;
}
