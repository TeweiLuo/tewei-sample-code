	.text
#ask for the highest number
addi $v0, $0, 4		#system call code for printing string
la $a0, enter_low	#low string to $a0
syscall			#print the question asking for lowest number

#store lowest numer
addi $v0, $0, 5		#system call code for reading an integer
syscall			#reader lowest number
add $t0, $v0, $0	#store lowest number in $s0

#ask for the highest number
add $v0, $0, 4		#system call code for printing string
la $a0, enter_high	#low string to $a0
syscall 		#print the question asking for highest number

#store the highest number
addi $v0, $0, 5		#system call code for reading an integer
syscall			#read highest number 
add $t1, $v0, $0	#store highest number in $s1

#store the values for "h(0x68)", "l(0x6c)", and "y"(0x79)

addi $s0, $0, 0x68
addi $s1, $0, 0x6c
addi $s2, $0, 0x79


loop:	

#Computer the guess and ask the user whether it is correct
add $t2, $t0, $t1	#compute the guess number by take the average of the highest and lowest number
sra $t2, $t2, 1		#and save it to $t2
			
addi $v0, $0, 4		#system call code for printing string
la $a0, is_your_number	#low string to $a0
syscall			#print the question asking whether the guess is correct

addi $v0, $0, 1		#system call code for printing an integer
add $a0, $t2, $0	#store guess to $a0
syscall			#print the guess

read_again:
# Read user answer
addi $v0, $0, 12		#system call code for reading a character
syscall			#reader user answer
add $t3, $v0, $0	#store user answer in $t3


beq $s2, $t3, yes	#notify if guess is correct

beq $s0, $t3, high	#update highest number in range if answer is h

beq $s1, $t3, low	#update lowest number in range if answer is l

#ask for a valid input if the answer if neither y, h, or l
addi $v0, $0, 4			#system call code for printing string
la $a0, ask_for_valid_input	#store string to $a0
syscall				#print the string
j read_again

high: 		
add $t1, $0, $t2	#update highest number in range to be the guess
j loop			#repeat the guess

low:	
add $t0, $0, $t2	#update lowest number in range to be the guess
j loop			#repeat the guess

yes:
addi $v0, $0, 4		#system call code for printing string
la $a0, yay		#load string "Yay!"
syscall			#print the string "Yay!"


	.data 

enter_low:	.asciiz"Please enter the lowest number in range:"
enter_high:	.asciiz"Please enter the highest number in range:"
think:		.asciiz"Please think of a number in your range."
is_your_number:	.asciiz"Is your number \n"
newline:	.asciiz"\n"
yay:		.asciiz"\n Yay!"

ask_for_valid_input: .asciiz"Please enter h, l, or y"
